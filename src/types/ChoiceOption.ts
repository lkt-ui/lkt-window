export type ChoiceOption = {
    label: string,
    value: string | number
}