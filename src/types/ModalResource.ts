import {LktObject} from "lkt-ts-interfaces";

export type ModalResource = {
    resource: string,
    params?: LktObject
}