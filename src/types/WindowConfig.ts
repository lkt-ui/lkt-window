import { VueElement } from 'vue';

export type WindowConfig = {
  alias: string;
  component: VueElement;
};
