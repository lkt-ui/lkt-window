import { LktObject } from 'lkt-ts-interfaces';

import {ValidModalKey} from "./types";

export type WindowCallerConfig = {
  alias: string;
  key: ValidModalKey;
  props?: LktObject;
};