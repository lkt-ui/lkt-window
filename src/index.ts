/* eslint-disable import/prefer-default-export */
import { App, Component } from 'vue';

import LktWindowCanvas from './components/LktWindowCanvas.vue';
import { addWindow, closeWindow, openWindow } from './functions/functions';
import LktAlertWindow from './lib-components/LktAlertWindow.vue';
import LktChoiceWindow from './lib-components/LktChoiceWindow.vue';
import LktConfirmWindow from './lib-components/LktConfirmWindow.vue';
import LktCRUDWindow from './lib-components/LktCRUDWindow.vue';
import { default as window } from './lib-components/LktWindow.vue';
import { Settings } from './settings/Settings';

const LktWindow = {
  install: (app: App, options: any) => {
    app
      .component('lkt-window-canvas', LktWindowCanvas)
      .component('lkt-crud-window', LktCRUDWindow)
      .component('lkt-alert-window', LktAlertWindow)
      .component('lkt-confirm-window', LktConfirmWindow)
      .component('lkt-choice-window', LktChoiceWindow)
      .component('lkt-window', window);
  },
  setCanvas: (component: Component) => {
    Settings.canvas = component;
  },
};

export default LktWindow;

export { addWindow, closeWindow, openWindow };
