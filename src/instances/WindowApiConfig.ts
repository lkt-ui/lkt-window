import {ModalResource} from "../types/ModalResource";
import {ResourceValue} from "../value-objects/ResourceValue";

export class WindowApiConfig {

    createResource: ResourceValue = new ResourceValue();
    readResource: ResourceValue = new ResourceValue();
    updateResource: ResourceValue = new ResourceValue();
    dropResource: ResourceValue = new ResourceValue();

    constructor(create?: ModalResource, read?: ModalResource, update?: ModalResource, drop?: ModalResource) {

        if (create) {
            this.createResource = new ResourceValue(create.resource);
        }

        if (read) {
            this.readResource = new ResourceValue(read.resource);
        }

        if (update) {
            this.updateResource = new ResourceValue(update.resource);
        }

        if (drop) {
            this.dropResource = new ResourceValue(drop.resource);
        }
    }

}