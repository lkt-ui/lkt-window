export class LoadingValue {
    value: boolean;

    constructor(value: boolean) {
        this.value = value;
    }

    start() {
        this.value = true;
    }

    finish() {
        this.value = false;
    }
}