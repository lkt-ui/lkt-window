export class ResourceValue {
    private readonly value: string;

    constructor(value?: string) {
        if (!value) {
            value = '';
        }
        this.value = value;
    }
}