import { LktObject } from 'lkt-ts-interfaces';
import { Component } from 'vue';

import { Settings } from '../settings/Settings';
import { ValidModalKey } from '../types/types';
import { WindowCallerConfig } from '../types/WindowCallerConfig';

export const openWindowConfig = (config: WindowCallerConfig) => {
  Settings.canvas.open(config.alias, config.key, config.props);
};

export const openWindow = (
  alias: string,
  key: ValidModalKey = '_',
  props: LktObject = {}
) => {
  Settings.controller.open(alias, key, props);
  Settings.canvas.refresh();
};
export const closeWindow = (alias: string, key: ValidModalKey = '_') => {
  Settings.controller.close(alias, key);
  Settings.canvas.refresh();
};

export const addWindow = (alias: string, component: Component) => {
  // @ts-ignore
  Settings.controller.addWindow({ alias, component });
};
