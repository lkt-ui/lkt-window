import { LktObject } from 'lkt-ts-interfaces';
import { reactive } from 'vue';

import { getInstanceIndex } from '../functions/config-functions';
import { RenderWindowStack } from '../interfaces/RenderWindowStack';
import { RenderWindowInfo } from '../types/RenderWindowInfo';
import { ValidModalKey } from '../types/types';
import { WindowConfig } from '../types/WindowConfig';

export class WindowController {
  private config: WindowConfig[] = [];
  readonly components: RenderWindowStack = {};
  private zIndex: number = 500;

  setConfig(configStack: WindowConfig[]) {
    this.config = configStack;
  }

  addWindow(configStack: WindowConfig) {
    this.config.push(configStack);
  }

  private findConfig(alias: string) {
    return this.config.find((z) => z.alias === alias);
  }

  private getWindowInfo(
    alias: string,
    key: ValidModalKey = '_',
    props: LktObject = {}
  ): RenderWindowInfo {
    const index = getInstanceIndex(alias, key),
      config = this.findConfig(alias);

    return {
      component: config.component,
      alias,
      index,
      key,
      props: { ...props, windowIndex: { alias, key } },
      zIndex: this.zIndex,
    };
  }

  open(alias: string, key: ValidModalKey = '_', props: LktObject = {}) {
    const config = this.findConfig(alias);
    if (config) {
      ++this.zIndex;
      const info = this.getWindowInfo(alias, key, props);
      if (this.components[info.index]) {
        return this.focus(info);
      }
      this.components[info.index] = info;
      return this.components[info.index];
    }
    return undefined;
  }

  private focus(info: RenderWindowInfo) {
    this.components[info.index].zIndex = this.zIndex;
    return this.components[info.index];
  }

  close(alias: string, key: ValidModalKey = '_') {
    const config = this.findConfig(alias);
    if (config) {
      --this.zIndex;
      const info = this.getWindowInfo(alias, key, {});
      delete this.components[info.index];

      if (Object.keys(this.components).length === 0) {
        this.zIndex = 500;
      }
    }
  }
}
