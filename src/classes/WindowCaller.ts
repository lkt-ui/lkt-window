import {LktObject} from "lkt-ts-interfaces";

import {ValidModalKey} from "../types/types";
import {WindowCallerConfig} from "../types/WindowCallerConfig";

export class WindowCaller {
    alias: string = '';
    key: ValidModalKey = '_';
    props: LktObject = {};

    constructor(config?: WindowCallerConfig) {
        if (config) {
            this.alias = config.alias;
            this.key = config.key;

            if (!config.props) {
                config.props = {};
            }
            this.props = config.props;
        }
    }

    isCallable() {
        return this.alias !== '';
    }
}