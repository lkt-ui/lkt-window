import { WindowController } from '../classes/WindowController';

export const Settings = {
  controller: new WindowController(),

  // @ts-ignore
  canvas: undefined,
};
