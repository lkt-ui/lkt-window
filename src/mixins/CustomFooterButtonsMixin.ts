import { getSlots } from 'lkt-vue-tools';

export const CustomFooterButtonsMixin = {
  props: {
    disabledButtons: { type: Array, default: (): string[] => [] },
  },
  data() {
    return { observedFooterButtons: false };
  },
  watch: {
    observedFooterButtons() {
      this.$forceUpdate();
    },
  },
  computed: {
    customFooterButtons() {
      this.observedFooterButtons = !this.observedFooterButtons; // Magic line, force refresh
      return getSlots(this.$slots, 'footer-button-');
    },
    hasCustomFooterButtons() {
      return Object.keys(this.customFooterButtons).length > 0;
    },
  },
  methods: {
    getFooterButtonSlotKey(key: string | number): string {
      return `footer-button-${key}`;
    },
  },
};
