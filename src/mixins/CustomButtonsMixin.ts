import {getSlots} from "lkt-vue-tools";

export const CustomButtonsMixin = {
    props: {
        showClose: {type: Boolean, default: true},
        disabledButtons: {type: Array, default: (): string[] => []}
    },
    data() {
        return {observedButtons: false}
    },
    computed: {
        customButtons() {
            this.observedButtons; // Magic line, force refresh
            return getSlots(this.$slots, 'button-');
        },
    },
    methods: {
        getButtonSlotKey(key: string | number): string {
            return `button-${key}`;
        },
    }
}