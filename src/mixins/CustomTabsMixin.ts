import { getSlots } from 'lkt-vue-tools';

export const CustomTabsMixin = {
  data() {
    return { observedTabs: false };
  },
  computed: {
    customTabs() {
      this.observedTabs; // Magic line, force refresh
      return getSlots(this.$slots, 'tab-');
    },
    hasCustomTabs() {
      return Object.keys(this.customTabs).length > 0;
    }
  },
  methods: {
    getTabSlotKey(key: string | number): string {
      return `tab-${key}`;
    },
  },
};
