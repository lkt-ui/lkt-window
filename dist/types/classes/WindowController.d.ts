import { LktObject } from 'lkt-ts-interfaces';
import { RenderWindowStack } from '../interfaces/RenderWindowStack';
import { RenderWindowInfo } from '../types/RenderWindowInfo';
import { ValidModalKey } from '../types/types';
import { WindowConfig } from '../types/WindowConfig';
export declare class WindowController {
    private config;
    readonly components: RenderWindowStack;
    private zIndex;
    setConfig(configStack: WindowConfig[]): void;
    addWindow(configStack: WindowConfig): void;
    private findConfig;
    private getWindowInfo;
    open(alias: string, key?: ValidModalKey, props?: LktObject): RenderWindowInfo;
    private focus;
    close(alias: string, key?: ValidModalKey): void;
}
