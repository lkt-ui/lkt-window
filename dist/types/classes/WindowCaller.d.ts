import { LktObject } from "lkt-ts-interfaces";
import { ValidModalKey } from "../types/types";
import { WindowCallerConfig } from "../types/WindowCallerConfig";
export declare class WindowCaller {
    alias: string;
    key: ValidModalKey;
    props: LktObject;
    constructor(config?: WindowCallerConfig);
    isCallable(): boolean;
}
