export declare const CustomButtonsMixin: {
    props: {
        showClose: {
            type: BooleanConstructor;
            default: boolean;
        };
        disabledButtons: {
            type: ArrayConstructor;
            default: () => string[];
        };
    };
    data(): {
        observedButtons: boolean;
    };
    computed: {
        customButtons(): import("lkt-ts-interfaces").LktObject;
    };
    methods: {
        getButtonSlotKey(key: string | number): string;
    };
};
