export declare const CustomTabsMixin: {
    data(): {
        observedTabs: boolean;
    };
    computed: {
        customTabs(): import("lkt-ts-interfaces").LktObject;
        hasCustomTabs(): boolean;
    };
    methods: {
        getTabSlotKey(key: string | number): string;
    };
};
