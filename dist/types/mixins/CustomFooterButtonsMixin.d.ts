export declare const CustomFooterButtonsMixin: {
    props: {
        disabledButtons: {
            type: ArrayConstructor;
            default: () => string[];
        };
    };
    data(): {
        observedFooterButtons: boolean;
    };
    watch: {
        observedFooterButtons(): void;
    };
    computed: {
        customFooterButtons(): import("lkt-ts-interfaces").LktObject;
        hasCustomFooterButtons(): boolean;
    };
    methods: {
        getFooterButtonSlotKey(key: string | number): string;
    };
};
