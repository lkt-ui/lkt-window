import { LktObject } from 'lkt-ts-interfaces';
import { Component } from 'vue';
import { ValidModalKey } from '../types/types';
import { WindowCallerConfig } from '../types/WindowCallerConfig';
export declare const openWindowConfig: (config: WindowCallerConfig) => void;
export declare const openWindow: (alias: string, key?: ValidModalKey, props?: LktObject) => void;
export declare const closeWindow: (alias: string, key?: ValidModalKey) => void;
export declare const addWindow: (alias: string, component: Component) => void;
