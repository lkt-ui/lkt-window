import { RenderWindowInfo } from '../types/RenderWindowInfo';
export interface RenderWindowStack {
    [key: string | number]: RenderWindowInfo;
}
