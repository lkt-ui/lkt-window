export declare type ChoiceOption = {
    label: string;
    value: string | number;
};
