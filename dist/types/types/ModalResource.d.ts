import { LktObject } from "lkt-ts-interfaces";
export declare type ModalResource = {
    resource: string;
    params?: LktObject;
};
