import { LktObject } from 'lkt-ts-interfaces';
import { VueElement } from "vue";
export declare type RenderWindowInfo = {
    component: VueElement;
    alias: string;
    index: string;
    key: string | number;
    props: LktObject;
    zIndex: number;
};
