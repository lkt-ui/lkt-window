import { VueElement } from 'vue';
export declare type WindowConfig = {
    alias: string;
    component: VueElement;
};
