export declare class LoadingValue {
    value: boolean;
    constructor(value: boolean);
    start(): void;
    finish(): void;
}
