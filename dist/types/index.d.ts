import { App, Component } from 'vue';
import { addWindow, closeWindow, openWindow } from './functions/functions';
declare const LktWindow: {
    install: (app: App, options: any) => void;
    setCanvas: (component: Component) => void;
};
export default LktWindow;
export { addWindow, closeWindow, openWindow };
