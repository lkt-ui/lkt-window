import { ModalResource } from "../types/ModalResource";
import { ResourceValue } from "../value-objects/ResourceValue";
export declare class WindowApiConfig {
    createResource: ResourceValue;
    readResource: ResourceValue;
    updateResource: ResourceValue;
    dropResource: ResourceValue;
    constructor(create?: ModalResource, read?: ModalResource, update?: ModalResource, drop?: ModalResource);
}
