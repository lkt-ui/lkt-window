import { RenderWindowInfo } from '../types/RenderWindowInfo';
declare const _default: {
    name: string;
    inheritAttrs: boolean;
    customOptions: {};
    methods: {};
    setup(): {
        refresher: import("vue").Ref<number>;
        refresh: () => void;
        components: import("vue").ComputedRef<RenderWindowInfo[]>;
        getStyle: (config: RenderWindowInfo) => string;
    };
};
export default _default;
