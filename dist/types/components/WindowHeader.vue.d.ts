declare const _default: {
    name: string;
    mixins: {
        props: {
            showClose: {
                type: BooleanConstructor;
                default: boolean;
            };
            disabledButtons: {
                type: ArrayConstructor;
                default: () => string[];
            };
        };
        data(): {
            observedButtons: boolean;
        };
        computed: {
            customButtons(): import("lkt-ts-interfaces").LktObject;
        };
        methods: {
            getButtonSlotKey(key: string | number): string;
        };
    }[];
    props: {
        preTitle: {
            type: StringConstructor;
            default: string;
        };
        title: {
            type: StringConstructor;
            default: string;
        };
    };
    methods: {
        onClose(e: Event): void;
    };
};
export default _default;
