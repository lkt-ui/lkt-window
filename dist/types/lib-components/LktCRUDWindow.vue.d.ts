import { PropType } from "vue";
import { ResourceCaller, ResourceCallerConfig } from "lkt-http";
import { LktObject } from "lkt-ts-interfaces";
import { WindowCallerConfig } from "../types/WindowCallerConfig";
import { DataState } from "lkt-data-state";
import { WindowCaller } from "../classes/WindowCaller";
import { LoadingValue } from "../value-objects/LoadingValue";
declare const _default: import("vue").DefineComponent<{
    palette: {
        type: StringConstructor;
        default: string;
    };
    size: {
        type: StringConstructor;
        default: string;
    };
    preTitle: {
        type: StringConstructor;
        default: string;
    };
    title: {
        type: StringConstructor;
        default: string;
    };
    loading: {
        type: BooleanConstructor;
        default: boolean;
    };
    showClose: {
        type: BooleanConstructor;
        default: boolean;
    };
    windowIndex: {
        type: ObjectConstructor;
        default: () => {
            alias: string;
            key: string;
        };
    };
    tabs: {
        type: ObjectConstructor;
        default: () => {};
    };
    createInitialData: {
        type: PropType<LktObject>;
        default: () => void;
    };
    beforeCreateConfirm: {
        type: PropType<WindowCallerConfig>;
        default: any;
    };
    beforeUpdateConfirm: {
        type: PropType<WindowCallerConfig>;
        default: any;
    };
    beforeDeleteConfirm: {
        type: PropType<WindowCallerConfig>;
        default: any;
    };
    createVisible: {
        type: BooleanConstructor;
        default: boolean;
    };
    updateVisible: {
        type: BooleanConstructor;
        default: boolean;
    };
    deleteVisible: {
        type: BooleanConstructor;
        default: boolean;
    };
    createDisabled: {
        type: BooleanConstructor;
        default: boolean;
    };
    updateDisabled: {
        type: BooleanConstructor;
        default: boolean;
    };
    deleteDisabled: {
        type: BooleanConstructor;
        default: boolean;
    };
    create: {
        type: PropType<ResourceCallerConfig>;
        default: () => {};
    };
    read: {
        type: PropType<ResourceCallerConfig>;
        default: () => {};
    };
    update: {
        type: PropType<ResourceCallerConfig>;
        default: () => {};
    };
    drop: {
        type: PropType<ResourceCallerConfig>;
        default: () => {};
    };
    validateCanRead: {
        type: FunctionConstructor;
        default: (data: LktObject) => true;
    };
    validateReadData: {
        type: FunctionConstructor;
        default: (data: LktObject) => true;
    };
}, unknown, {
    style: string;
    activeTab: string;
    loadingStatus: LoadingValue;
    data: {};
    createCaller: ResourceCaller;
    readCaller: ResourceCaller;
    updateCaller: ResourceCaller;
    deleteCaller: ResourceCaller;
    dataState: DataState;
    confirmCreate: WindowCaller;
    confirmUpdate: WindowCaller;
    confirmDelete: WindowCaller;
}, {
    hasData(): boolean;
    hasCreateDisabled(): boolean;
    hasUpdateDisabled(): boolean;
    hasDeleteDisabled(): any;
}, {
    callAPI(resourceCaller: ResourceCaller, confirmCaller: WindowCaller, confirmOptions: WindowCallerConfig): void;
    onCreate(): void;
    onUpdate(): void;
    onDelete(): void;
    load(): void;
}, {
    data(): {
        observedTabs: boolean;
    };
    computed: {
        customTabs(): LktObject;
        hasCustomTabs(): boolean;
    };
    methods: {
        getTabSlotKey(key: string | number): string;
    };
}, import("vue").ComponentOptionsMixin, ("loading" | "loaded" | "update" | "delete" | "create" | "create-fail" | "update-fail" | "delete-fail" | "read" | "read-fail" | "read-invalid")[], "loaded" | "loading" | "update" | "delete" | "create" | "create-fail" | "update-fail" | "delete-fail" | "read" | "read-fail" | "read-invalid", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    palette: {
        type: StringConstructor;
        default: string;
    };
    size: {
        type: StringConstructor;
        default: string;
    };
    preTitle: {
        type: StringConstructor;
        default: string;
    };
    title: {
        type: StringConstructor;
        default: string;
    };
    loading: {
        type: BooleanConstructor;
        default: boolean;
    };
    showClose: {
        type: BooleanConstructor;
        default: boolean;
    };
    windowIndex: {
        type: ObjectConstructor;
        default: () => {
            alias: string;
            key: string;
        };
    };
    tabs: {
        type: ObjectConstructor;
        default: () => {};
    };
    createInitialData: {
        type: PropType<LktObject>;
        default: () => void;
    };
    beforeCreateConfirm: {
        type: PropType<WindowCallerConfig>;
        default: any;
    };
    beforeUpdateConfirm: {
        type: PropType<WindowCallerConfig>;
        default: any;
    };
    beforeDeleteConfirm: {
        type: PropType<WindowCallerConfig>;
        default: any;
    };
    createVisible: {
        type: BooleanConstructor;
        default: boolean;
    };
    updateVisible: {
        type: BooleanConstructor;
        default: boolean;
    };
    deleteVisible: {
        type: BooleanConstructor;
        default: boolean;
    };
    createDisabled: {
        type: BooleanConstructor;
        default: boolean;
    };
    updateDisabled: {
        type: BooleanConstructor;
        default: boolean;
    };
    deleteDisabled: {
        type: BooleanConstructor;
        default: boolean;
    };
    create: {
        type: PropType<ResourceCallerConfig>;
        default: () => {};
    };
    read: {
        type: PropType<ResourceCallerConfig>;
        default: () => {};
    };
    update: {
        type: PropType<ResourceCallerConfig>;
        default: () => {};
    };
    drop: {
        type: PropType<ResourceCallerConfig>;
        default: () => {};
    };
    validateCanRead: {
        type: FunctionConstructor;
        default: (data: LktObject) => true;
    };
    validateReadData: {
        type: FunctionConstructor;
        default: (data: LktObject) => true;
    };
}>> & {
    onLoading?: (...args: any[]) => any;
    onLoaded?: (...args: any[]) => any;
    onUpdate?: (...args: any[]) => any;
    onDelete?: (...args: any[]) => any;
    onCreate?: (...args: any[]) => any;
    "onCreate-fail"?: (...args: any[]) => any;
    "onUpdate-fail"?: (...args: any[]) => any;
    "onDelete-fail"?: (...args: any[]) => any;
    onRead?: (...args: any[]) => any;
    "onRead-fail"?: (...args: any[]) => any;
    "onRead-invalid"?: (...args: any[]) => any;
}, {
    loading: boolean;
    title: string;
    drop: ResourceCallerConfig;
    size: string;
    update: ResourceCallerConfig;
    windowIndex: Record<string, any>;
    palette: string;
    preTitle: string;
    showClose: boolean;
    tabs: Record<string, any>;
    create: ResourceCallerConfig;
    read: ResourceCallerConfig;
    createVisible: boolean;
    updateVisible: boolean;
    deleteVisible: boolean;
    createInitialData: void;
    beforeCreateConfirm: WindowCallerConfig;
    beforeUpdateConfirm: WindowCallerConfig;
    beforeDeleteConfirm: WindowCallerConfig;
    createDisabled: boolean;
    updateDisabled: boolean;
    deleteDisabled: boolean;
    validateCanRead: Function;
    validateReadData: Function;
}>;
export default _default;
