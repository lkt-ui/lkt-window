declare const _default: import("vue").DefineComponent<{
    palette: {
        type: StringConstructor;
        default: string;
    };
    size: {
        type: StringConstructor;
        default: string;
    };
    preTitle: {
        type: StringConstructor;
        default: string;
    };
    title: {
        type: StringConstructor;
        default: string;
    };
    loading: {
        type: BooleanConstructor;
        default: boolean;
    };
    showClose: {
        type: BooleanConstructor;
        default: boolean;
    };
    disabledVeilClick: {
        type: BooleanConstructor;
        default: boolean;
    };
    windowIndex: {
        type: ObjectConstructor;
        default: () => {
            alias: string;
            key: string;
        };
    };
    tabs: {
        type: ObjectConstructor;
        default: () => {};
    };
}, unknown, {
    activeTab: string;
}, {
    classes(): string;
}, {
    onClose(): void;
    onVeilClick(): void;
}, {
    props: {
        disabledButtons: {
            type: ArrayConstructor;
            default: () => string[];
        };
    };
    data(): {
        observedFooterButtons: boolean;
    };
    watch: {
        observedFooterButtons(): void;
    };
    computed: {
        customFooterButtons(): import("lkt-ts-interfaces").LktObject;
        hasCustomFooterButtons(): boolean;
    };
    methods: {
        getFooterButtonSlotKey(key: string | number): string;
    };
} | {
    props: {
        showClose: {
            type: BooleanConstructor;
            default: boolean;
        };
        disabledButtons: {
            type: ArrayConstructor;
            default: () => string[];
        };
    };
    data(): {
        observedButtons: boolean;
    };
    computed: {
        customButtons(): import("lkt-ts-interfaces").LktObject;
    };
    methods: {
        getButtonSlotKey(key: string | number): string;
    };
} | {
    data(): {
        observedTabs: boolean;
    };
    computed: {
        customTabs(): import("lkt-ts-interfaces").LktObject;
        hasCustomTabs(): boolean;
    };
    methods: {
        getTabSlotKey(key: string | number): string;
    };
}, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    palette: {
        type: StringConstructor;
        default: string;
    };
    size: {
        type: StringConstructor;
        default: string;
    };
    preTitle: {
        type: StringConstructor;
        default: string;
    };
    title: {
        type: StringConstructor;
        default: string;
    };
    loading: {
        type: BooleanConstructor;
        default: boolean;
    };
    showClose: {
        type: BooleanConstructor;
        default: boolean;
    };
    disabledVeilClick: {
        type: BooleanConstructor;
        default: boolean;
    };
    windowIndex: {
        type: ObjectConstructor;
        default: () => {
            alias: string;
            key: string;
        };
    };
    tabs: {
        type: ObjectConstructor;
        default: () => {};
    };
}>>, {
    loading: boolean;
    title: string;
    size: string;
    windowIndex: Record<string, any>;
    palette: string;
    preTitle: string;
    showClose: boolean;
    disabledVeilClick: boolean;
    tabs: Record<string, any>;
}>;
export default _default;
