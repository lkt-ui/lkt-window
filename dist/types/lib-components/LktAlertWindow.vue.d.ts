declare const _default: import("vue").DefineComponent<{
    palette: {
        type: StringConstructor;
        default: string;
    };
    size: {
        type: StringConstructor;
        default: string;
    };
    preTitle: {
        type: StringConstructor;
        default: string;
    };
    title: {
        type: StringConstructor;
        default: string;
    };
    loading: {
        type: BooleanConstructor;
        default: boolean;
    };
    showClose: {
        type: BooleanConstructor;
        default: boolean;
    };
    windowIndex: {
        type: ObjectConstructor;
        default: () => {
            alias: string;
            key: string;
        };
    };
}, unknown, unknown, {}, {
    onClose(): void;
}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    palette: {
        type: StringConstructor;
        default: string;
    };
    size: {
        type: StringConstructor;
        default: string;
    };
    preTitle: {
        type: StringConstructor;
        default: string;
    };
    title: {
        type: StringConstructor;
        default: string;
    };
    loading: {
        type: BooleanConstructor;
        default: boolean;
    };
    showClose: {
        type: BooleanConstructor;
        default: boolean;
    };
    windowIndex: {
        type: ObjectConstructor;
        default: () => {
            alias: string;
            key: string;
        };
    };
}>>, {
    loading: boolean;
    title: string;
    size: string;
    windowIndex: Record<string, any>;
    palette: string;
    preTitle: string;
    showClose: boolean;
}>;
export default _default;
