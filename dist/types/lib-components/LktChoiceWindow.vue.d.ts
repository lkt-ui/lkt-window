import { PropType } from 'vue';
import { ChoiceOption } from '../types/ChoiceOption';
declare const _default: import("vue").DefineComponent<{
    palette: {
        type: StringConstructor;
        default: string;
    };
    size: {
        type: StringConstructor;
        default: string;
    };
    preTitle: {
        type: StringConstructor;
        default: string;
    };
    title: {
        type: StringConstructor;
        default: string;
    };
    loading: {
        type: BooleanConstructor;
        default: boolean;
    };
    showClose: {
        type: BooleanConstructor;
        default: boolean;
    };
    windowIndex: {
        type: ObjectConstructor;
        default: () => {
            alias: string;
            key: string;
        };
    };
    options: {
        type: PropType<ChoiceOption[]>;
        default: () => ChoiceOption[];
    };
}, unknown, unknown, {
    hasOptionSlot(): boolean;
}, {
    onClose(): void;
    onChoice(opt: ChoiceOption): void;
}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "choice"[], "choice", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    palette: {
        type: StringConstructor;
        default: string;
    };
    size: {
        type: StringConstructor;
        default: string;
    };
    preTitle: {
        type: StringConstructor;
        default: string;
    };
    title: {
        type: StringConstructor;
        default: string;
    };
    loading: {
        type: BooleanConstructor;
        default: boolean;
    };
    showClose: {
        type: BooleanConstructor;
        default: boolean;
    };
    windowIndex: {
        type: ObjectConstructor;
        default: () => {
            alias: string;
            key: string;
        };
    };
    options: {
        type: PropType<ChoiceOption[]>;
        default: () => ChoiceOption[];
    };
}>> & {
    onChoice?: (...args: any[]) => any;
}, {
    loading: boolean;
    title: string;
    options: ChoiceOption[];
    size: string;
    windowIndex: Record<string, any>;
    palette: string;
    preTitle: string;
    showClose: boolean;
}>;
export default _default;
