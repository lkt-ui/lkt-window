var M = Object.defineProperty;
var N = (e, t, o) => t in e ? M(e, t, { enumerable: !0, configurable: !0, writable: !0, value: o }) : e[t] = o;
var w = (e, t, o) => (N(e, typeof t != "symbol" ? t + "" : t, o), o);
import { ref as E, getCurrentInstance as H, computed as q, openBlock as n, createElementBlock as i, Fragment as S, renderList as C, createBlock as _, resolveDynamicComponent as G, mergeProps as J, createElementVNode as v, toDisplayString as V, createCommentVNode as f, renderSlot as p, withModifiers as A, defineComponent as T, resolveComponent as m, normalizeClass as Q, createVNode as y, createSlots as W, withCtx as l, withDirectives as B, vShow as I, createTextVNode as g } from "vue";
import { getSlots as L, slotProvided as X } from "lkt-vue-tools";
import { ResourceCaller as b } from "lkt-http";
import { DataState as U } from "lkt-data-state";
const Y = (e, t = "_") => `${e}_${t}`;
class Z {
  constructor() {
    w(this, "config", []);
    w(this, "components", {});
    w(this, "zIndex", 500);
  }
  setConfig(t) {
    this.config = t;
  }
  addWindow(t) {
    this.config.push(t);
  }
  findConfig(t) {
    return this.config.find((o) => o.alias === t);
  }
  getWindowInfo(t, o = "_", s = {}) {
    const d = Y(t, o);
    return {
      component: this.findConfig(t).component,
      alias: t,
      index: d,
      key: o,
      props: { ...s, windowIndex: { alias: t, key: o } },
      zIndex: this.zIndex
    };
  }
  open(t, o = "_", s = {}) {
    if (this.findConfig(t)) {
      ++this.zIndex;
      const r = this.getWindowInfo(t, o, s);
      return this.components[r.index] ? this.focus(r) : (this.components[r.index] = r, this.components[r.index]);
    }
  }
  focus(t) {
    return this.components[t.index].zIndex = this.zIndex, this.components[t.index];
  }
  close(t, o = "_") {
    if (this.findConfig(t)) {
      --this.zIndex;
      const d = this.getWindowInfo(t, o, {});
      delete this.components[d.index], Object.keys(this.components).length === 0 && (this.zIndex = 500);
    }
  }
}
const $ = {
  controller: new Z(),
  canvas: void 0
}, x = {
  name: "LktWindowCanvas",
  inheritAttrs: !1,
  customOptions: {},
  methods: {},
  setup() {
    const e = E(0), { ctx: t } = H(), o = () => {
      e.value = e.value + 1, setTimeout(() => {
        t.$forceUpdate();
      }, 1);
    }, s = (r) => {
      const a = [];
      return r.zIndex && a.push(`z-index: ${r.zIndex}`), a.join(";");
    }, d = q(() => (e.value, Object.values($.controller.components)));
    return {
      refresher: e,
      refresh: o,
      components: d,
      getStyle: s
    };
  }
}, k = (e, t) => {
  const o = e.__vccOpts || e;
  for (const [s, d] of t)
    o[s] = d;
  return o;
}, ee = { class: "lkt-window-canvas" };
function te(e, t, o, s, d, r) {
  return n(), i("section", ee, [
    (n(!0), i(S, null, C(s.components, (a) => (n(), _(G(a.component), J({
      key: a.index
    }, a.props, {
      style: s.getStyle(a)
    }), null, 16, ["style"]))), 128))
  ]);
}
const oe = /* @__PURE__ */ k(x, [["render", te]]), Ne = (e, t = "_", o = {}) => {
  $.controller.open(e, t, o), $.canvas.refresh();
}, O = (e, t = "_") => {
  $.controller.close(e, t), $.canvas.refresh();
}, Ee = (e, t) => {
  $.controller.addWindow({ alias: e, component: t });
}, K = {
  props: {
    showClose: { type: Boolean, default: !0 },
    disabledButtons: { type: Array, default: () => [] }
  },
  data() {
    return { observedButtons: !1 };
  },
  computed: {
    customButtons() {
      return this.observedButtons, L(this.$slots, "button-");
    }
  },
  methods: {
    getButtonSlotKey(e) {
      return `button-${e}`;
    }
  }
}, ne = {
  name: "WindowHeader",
  mixins: [K],
  props: {
    preTitle: { type: String, default: "" },
    title: { type: String, default: "" }
  },
  methods: {
    onClose(e) {
      this.$emit("close", e);
    }
  }
}, se = { class: "lkt-window-header" }, ae = { class: "lkt-window-header__title-container" }, ie = {
  key: 0,
  class: "lkt-window-header__pre-title"
}, le = {
  key: 1,
  class: "lkt-window-header__title"
}, re = ["data-button"], de = ["disabled"];
function ue(e, t, o, s, d, r) {
  return n(), i("header", se, [
    v("div", ae, [
      o.preTitle ? (n(), i("div", ie, V(o.preTitle), 1)) : f("", !0),
      o.title ? (n(), i("div", le, V(o.title), 1)) : f("", !0)
    ]),
    (n(), i("div", {
      "data-role": "button-tray",
      key: e.observedButtons
    }, [
      (n(!0), i(S, null, C(e.customButtons, (a, u) => (n(), i("div", { "data-button": u }, [
        p(e.$slots, u)
      ], 8, re))), 256)),
      e.showClose ? (n(), i("button", {
        key: 0,
        "data-button": "button-close",
        onClick: t[0] || (t[0] = A((...a) => r.onClose && r.onClose(...a), ["prevent", "stop"])),
        disabled: e.disabledButtons.indexOf("button-close") > -1
      }, null, 8, de)) : f("", !0)
    ]))
  ]);
}
const D = /* @__PURE__ */ k(ne, [["render", ue]]), R = {
  props: {
    disabledButtons: { type: Array, default: () => [] }
  },
  data() {
    return { observedFooterButtons: !1 };
  },
  watch: {
    observedFooterButtons() {
      this.$forceUpdate();
    }
  },
  computed: {
    customFooterButtons() {
      return this.observedFooterButtons = !this.observedFooterButtons, L(this.$slots, "footer-button-");
    },
    hasCustomFooterButtons() {
      return Object.keys(this.customFooterButtons).length > 0;
    }
  },
  methods: {
    getFooterButtonSlotKey(e) {
      return `footer-button-${e}`;
    }
  }
}, ce = {
  name: "WindowFooter",
  mixins: [R]
}, pe = {
  key: 0,
  class: "footer__footer"
}, he = {
  key: 1,
  class: "footer__button-tray"
}, fe = ["data-button"];
function me(e, t, o, s, d, r) {
  return e.customFooterButtons || !!e.$slots.footer ? (n(), i("footer", { key: e.observedFooterButtons }, [
    e.$slots.footer ? (n(), i("div", pe, [
      p(e.$slots, "footer")
    ])) : f("", !0),
    Object.keys(e.customFooterButtons).length > 0 ? (n(), i("div", he, [
      (n(!0), i(S, null, C(e.customFooterButtons, (a, u) => (n(), i("div", {
        key: u,
        "data-button": u
      }, [
        p(e.$slots, "footer-button-" + u)
      ], 8, fe))), 128))
    ])) : f("", !0)
  ])) : f("", !0);
}
const j = /* @__PURE__ */ k(ce, [["render", me]]), P = {
  data() {
    return { observedTabs: !1 };
  },
  computed: {
    customTabs() {
      return this.observedTabs, L(this.$slots, "tab-");
    },
    hasCustomTabs() {
      return Object.keys(this.customTabs).length > 0;
    }
  },
  methods: {
    getTabSlotKey(e) {
      return `tab-${e}`;
    }
  }
}, we = T({
  name: "LktWindow",
  components: { WindowHeader: D },
  mixins: [K, R, P],
  props: {
    palette: { type: String, default: "" },
    size: { type: String, default: "" },
    preTitle: { type: String, default: "" },
    title: { type: String, default: "" },
    loading: { type: Boolean, default: !1 },
    showClose: { type: Boolean, default: !0 },
    disabledVeilClick: { type: Boolean, default: !1 },
    windowIndex: {
      type: Object,
      default: () => ({ alias: "", key: "_" })
    },
    tabs: {
      type: Object,
      default: () => ({})
    }
  },
  data() {
    return {
      activeTab: ""
    };
  },
  computed: {
    classes() {
      const e = ["lkt-window"];
      return this.size && e.push(`is-${this.size}`), e.join(" ");
    }
  },
  watch: {
    $slots: {
      handler(e) {
        console.log("Updated slots", e);
      },
      deep: !0
    }
  },
  methods: {
    onClose() {
      O(this.windowIndex.alias, this.windowIndex.key);
    },
    onVeilClick() {
      this.disabledVeilClick || this.onClose();
    }
  },
  mounted() {
    !this.activeTab && this.customTabs && (this.activeTab = Object.keys(this.customTabs)[0]);
  }
}), be = {
  class: "lkt-window__inner",
  ref: "inner"
}, Ce = { "data-role": "loader" }, ye = { "data-role": "content" }, ge = { key: 0 }, ke = {
  key: 0,
  class: "footer__footer"
}, $e = {
  key: 1,
  class: "footer__button-tray"
};
function ve(e, t, o, s, d, r) {
  const a = m("window-header"), u = m("lkt-tabs");
  return n(), i("section", {
    class: Q(e.classes)
  }, [
    v("div", {
      class: "lkt-window__back",
      onClick: t[0] || (t[0] = A((...h) => e.onVeilClick && e.onVeilClick(...h), ["prevent", "stop"]))
    }),
    v("div", be, [
      y(a, {
        "pre-title": e.preTitle,
        title: e.title,
        "show-close": e.showClose,
        onClose: e.onClose
      }, W({ _: 2 }, [
        C(e.customButtons, (h, c) => ({
          name: e.getButtonSlotKey(c),
          fn: l(() => [
            p(e.$slots, e.getButtonSlotKey(c))
          ])
        }))
      ]), 1032, ["pre-title", "title", "show-close", "onClose"]),
      B(v("section", Ce, [
        p(e.$slots, "loader")
      ], 512), [
        [I, e.loading]
      ]),
      B(v("section", ye, [
        e.customTabs ? (n(), _(u, {
          key: 0,
          ref: "tabs",
          modelValue: e.activeTab,
          "onUpdate:modelValue": t[1] || (t[1] = (h) => e.activeTab = h),
          titles: e.tabs
        }, W({ _: 2 }, [
          C(e.customTabs, (h, c) => ({
            name: e.getTabSlotKey(c),
            fn: l(() => [
              p(e.$slots, e.getTabSlotKey(c))
            ])
          }))
        ]), 1032, ["modelValue", "titles"])) : f("", !0)
      ], 512), [
        [I, !e.loading]
      ]),
      e.customFooterButtons || !!e.$slots.footer ? (n(), i("footer", ge, [
        e.$slots.footer ? (n(), i("div", ke, [
          p(e.$slots, "footer")
        ])) : f("", !0),
        e.hasCustomFooterButtons ? (n(), i("div", $e, [
          (n(!0), i(S, null, C(e.customFooterButtons, (h, c) => p(e.$slots, "footer-button-" + c, {
            key: c,
            dataButton: c
          })), 128))
        ])) : f("", !0)
      ])) : f("", !0)
    ], 512)
  ], 2);
}
const z = /* @__PURE__ */ k(we, [["render", ve]]), Se = T({
  name: "LktAlertWindow",
  components: { LktWindow: z, WindowFooter: j, WindowHeader: D },
  props: {
    palette: { type: String, default: "" },
    size: { type: String, default: "" },
    preTitle: { type: String, default: "" },
    title: { type: String, default: "" },
    loading: { type: Boolean, default: !1 },
    showClose: { type: Boolean, default: !0 },
    windowIndex: {
      type: Object,
      default: () => ({ alias: "", key: "_" })
    }
  },
  methods: {
    onClose() {
      O(this.windowIndex.alias, this.windowIndex.key);
    }
  }
});
function _e(e, t, o, s, d, r) {
  const a = m("lkt-button"), u = m("lkt-window");
  return n(), _(u, {
    class: "type-alert",
    title: e.title,
    "pre-title": e.preTitle,
    palette: e.palette,
    size: e.size,
    loading: e.loading,
    windowIndex: e.windowIndex
  }, {
    "tab-alert": l(() => [
      p(e.$slots, "default")
    ]),
    "footer-button-confirm": l(() => [
      y(a, {
        onClick: e.onClose,
        palette: "success"
      }, {
        default: l(() => [
          g("Confirm")
        ]),
        _: 1
      }, 8, ["onClick"])
    ]),
    _: 3
  }, 8, ["title", "pre-title", "palette", "size", "loading", "windowIndex"]);
}
const Be = /* @__PURE__ */ k(Se, [["render", _e]]), Ie = T({
  name: "LktChoiceWindow",
  emits: ["choice"],
  components: { LktWindow: z, WindowFooter: j, WindowHeader: D },
  props: {
    palette: { type: String, default: "" },
    size: { type: String, default: "" },
    preTitle: { type: String, default: "" },
    title: { type: String, default: "" },
    loading: { type: Boolean, default: !1 },
    showClose: { type: Boolean, default: !0 },
    windowIndex: {
      type: Object,
      default: () => ({ alias: "", key: "_" })
    },
    options: {
      type: Array,
      default: () => []
    }
  },
  computed: {
    hasOptionSlot() {
      return X(this, "option");
    }
  },
  methods: {
    onClose() {
      O(this.windowIndex.alias, this.windowIndex.key);
    },
    onChoice(e) {
      this.$emit("choice", e), this.onClose();
    }
  }
}), Te = ["onClick"];
function De(e, t, o, s, d, r) {
  const a = m("lkt-button"), u = m("lkt-window");
  return n(), _(u, {
    class: "type-choice",
    title: e.title,
    "pre-title": e.preTitle,
    palette: e.palette,
    size: e.size,
    loading: e.loading,
    windowIndex: e.windowIndex
  }, {
    "tab-choice": l(() => [
      v("ul", null, [
        (n(!0), i(S, null, C(e.options, (h) => (n(), i("li", {
          onClick: (c) => e.onChoice(h)
        }, [
          e.hasOptionSlot ? p(e.$slots, "option", {
            key: 0,
            option: h
          }) : (n(), i(S, { key: 1 }, [
            g(V(h.label), 1)
          ], 64))
        ], 8, Te))), 256))
      ])
    ]),
    "footer-button-cancel": l(() => [
      y(a, {
        onClick: e.onClose,
        palette: "danger"
      }, {
        default: l(() => [
          g("Cancel")
        ]),
        _: 1
      }, 8, ["onClick"])
    ]),
    _: 3
  }, 8, ["title", "pre-title", "palette", "size", "loading", "windowIndex"]);
}
const ze = /* @__PURE__ */ k(Ie, [["render", De]]), We = T({
  name: "LktConfirmWindow",
  components: { LktWindow: z, WindowFooter: j, WindowHeader: D },
  props: {
    palette: { type: String, default: "" },
    size: { type: String, default: "" },
    preTitle: { type: String, default: "" },
    title: { type: String, default: "" },
    loading: { type: Boolean, default: !1 },
    showClose: { type: Boolean, default: !0 },
    windowIndex: {
      type: Object,
      default: () => ({ alias: "", key: "_" })
    },
    onConfirm: { type: Function, default: null }
  },
  methods: {
    onClose() {
      O(this.windowIndex.alias, this.windowIndex.key);
    },
    clickConfirm() {
      this.$emit("confirm"), this.onClose();
    }
  }
});
function Oe(e, t, o, s, d, r) {
  const a = m("lkt-button"), u = m("lkt-window");
  return n(), _(u, {
    class: "type-confirm",
    title: e.title,
    "pre-title": e.preTitle,
    palette: e.palette,
    size: e.size,
    loading: e.loading,
    windowIndex: e.windowIndex,
    "show-close": e.showClose
  }, W({
    "tab-confirm": l(() => [
      p(e.$slots, "default")
    ]),
    "footer-button-confirm": l(() => [
      y(a, {
        onClick: e.clickConfirm,
        palette: "success"
      }, {
        default: l(() => [
          g("Confirm")
        ]),
        _: 1
      }, 8, ["onClick"])
    ]),
    _: 2
  }, [
    e.showClose ? {
      name: "footer-button-cancel",
      fn: l(() => [
        y(a, {
          onClick: e.onClose,
          palette: "danger"
        }, {
          default: l(() => [
            g("Cancel")
          ]),
          _: 1
        }, 8, ["onClick"])
      ]),
      key: "0"
    } : void 0
  ]), 1032, ["title", "pre-title", "palette", "size", "loading", "windowIndex", "show-close"]);
}
const je = /* @__PURE__ */ k(We, [["render", Oe]]);
class F {
  constructor(t) {
    w(this, "alias", "");
    w(this, "key", "_");
    w(this, "props", {});
    t && (this.alias = t.alias, this.key = t.key, t.props || (t.props = {}), this.props = t.props);
  }
  isCallable() {
    return this.alias !== "";
  }
}
class Fe {
  constructor(t) {
    w(this, "value");
    this.value = t;
  }
  start() {
    this.value = !0;
  }
  finish() {
    this.value = !1;
  }
}
const Ve = T({
  name: "LktCrudWindow",
  emits: ["create", "create-fail", "update", "update-fail", "delete", "delete-fail", "read", "read-fail", "read-invalid", "loading", "loaded"],
  mixins: [P],
  components: { LktWindow: z, WindowFooter: j, WindowHeader: D },
  props: {
    palette: { type: String, default: "" },
    size: { type: String, default: "" },
    preTitle: { type: String, default: "" },
    title: { type: String, default: "" },
    loading: { type: Boolean, default: !1 },
    showClose: { type: Boolean, default: !0 },
    windowIndex: {
      type: Object,
      default: () => ({ alias: "", key: "_" })
    },
    tabs: {
      type: Object,
      default: () => ({})
    },
    createInitialData: { type: Object, default: () => {
    } },
    beforeCreateConfirm: { type: Object, default: null },
    beforeUpdateConfirm: { type: Object, default: null },
    beforeDeleteConfirm: { type: Object, default: null },
    createVisible: { type: Boolean, default: !1 },
    updateVisible: { type: Boolean, default: !1 },
    deleteVisible: { type: Boolean, default: !1 },
    createDisabled: { type: Boolean, default: !1 },
    updateDisabled: { type: Boolean, default: !1 },
    deleteDisabled: { type: Boolean, default: !1 },
    create: {
      type: Object,
      default: () => ({})
    },
    read: {
      type: Object,
      default: () => ({})
    },
    update: {
      type: Object,
      default: () => ({})
    },
    drop: {
      type: Object,
      default: () => ({})
    },
    validateCanRead: { type: Function, default: (e) => !0 },
    validateReadData: { type: Function, default: (e) => !0 }
  },
  data() {
    const e = {};
    return {
      style: "",
      activeTab: "",
      loadingStatus: new Fe(this.loading),
      data: e,
      createCaller: new b(this.create),
      readCaller: new b(this.read),
      updateCaller: new b(this.update),
      deleteCaller: new b(this.drop),
      dataState: new U(e),
      confirmCreate: new F(this.beforeCreateConfirm),
      confirmUpdate: new F(this.beforeUpdateConfirm),
      confirmDelete: new F(this.beforeDeleteConfirm)
    };
  },
  computed: {
    hasData() {
      return this.data ? Object.keys(this.data).length > 0 : !1;
    },
    hasCreateDisabled() {
      return !this.dataState.isChanged;
    },
    hasUpdateDisabled() {
      return !this.dataState.isChanged;
    },
    hasDeleteDisabled() {
      return this.dataState.isChanged;
    }
  },
  watch: {
    data: {
      handler(e) {
        this.dataState.store(e);
      },
      deep: !0
    },
    loading: {
      handler(e) {
        e ? this.loadingStatus.start() : this.loadingStatus.finish();
      },
      deep: !0
    },
    create: {
      handler(e) {
        this.createCaller = new b(this.create);
      },
      deep: !0
    },
    read: {
      handler(e) {
        this.readCaller = new b(this.read);
      },
      deep: !0
    },
    update: {
      handler(e) {
        this.updateCaller = new b(this.update);
      },
      deep: !0
    },
    drop: {
      handler(e) {
        this.deleteCaller = new b(this.drop);
      },
      deep: !0
    },
    readCaller: {
      handler(e) {
        this.load();
      },
      deep: !0
    },
    createVisible() {
      this.$forceUpdate();
    },
    updateVisible() {
      this.$forceUpdate();
    },
    deleteVisible() {
      this.$forceUpdate();
    }
  },
  methods: {
    callAPI(e, t, o) {
      if (t.isCallable()) {
        const s = { ...o };
        s.props = {
          ...s.props,
          onConfirm: () => {
            this.loadingStatus.start(), this.$nextTick(() => {
              e.call().then((d) => {
                this.loadingStatus.finish(), this.$emit("create", d);
              }).catch(() => {
                this.loadingStatus.finish(), this.$emit("create-fail");
              });
            });
          }
        }, this.$root.openWindowConfig(s);
        return;
      }
      this.loadingStatus.start(), this.$emit("loading"), this.$nextTick(() => {
        e.call().then((s) => {
          this.loadingStatus.finish(), this.$emit("create", s), this.$emit("loaded");
        }).catch(() => {
          this.loadingStatus.finish(), this.$emit("create-fail"), this.$emit("loaded");
        });
      });
    },
    onCreate() {
      this.callAPI(this.createCaller, this.confirmCreate, this.beforeCreateConfirm);
    },
    onUpdate() {
      this.updateCaller.setParams(this.data), this.callAPI(this.updateCaller, this.confirmUpdate, this.beforeUpdateConfirm);
    },
    onDelete() {
      this.updateCaller.setParams(this.data), this.callAPI(this.deleteCaller, this.confirmDelete, this.beforeDeleteConfirm);
    },
    load() {
      let e = !0;
      if (typeof this.validateCanRead == "function" && (e = this.validateCanRead(this.readCaller) === !0), !e) {
        this.data = { ...this.createInitialData }, this.$emit("read-invalid");
        return;
      }
      if (!this.readCaller && !this.readCaller.isCallable()) {
        this.data = { ...this.createInitialData }, this.$emit("read-invalid");
        return;
      }
      this.loadingStatus.start(), this.$emit("loading"), this.$nextTick(() => {
        this.readCaller.call().then((t) => {
          let o = !0;
          if (typeof this.validateReadData == "function")
            try {
              o = this.validateReadData(t) === !0;
            } catch {
              console.error("An error occurred while validating response");
            }
          o ? (this.data = t, this.dataState = new U(t), this.loadingStatus.finish(), this.$emit("read", this.data), this.$emit("loaded")) : (this.$emit("read-fail"), this.$emit("loaded"));
        }).catch(() => {
          this.$emit("read-fail"), this.$emit("loaded");
        });
      });
    }
  },
  mounted() {
    this.load();
  }
});
function Le(e, t, o, s, d, r) {
  const a = m("lkt-button"), u = m("lkt-window");
  return n(), _(u, {
    class: "type-crud",
    title: e.title,
    "pre-title": e.preTitle,
    palette: e.palette,
    size: e.size,
    loading: e.loadingStatus.value,
    windowIndex: e.windowIndex,
    tabs: e.tabs
  }, W({
    "footer-button-create": l(() => [
      B(y(a, {
        onClick: e.onCreate,
        palette: "success",
        disabled: e.hasCreateDisabled
      }, {
        default: l(() => [
          g("Create ")
        ]),
        _: 1
      }, 8, ["onClick", "disabled"]), [
        [I, !e.loadingStatus.value && e.createVisible]
      ])
    ]),
    "footer-button-update": l(() => [
      B(y(a, {
        onClick: e.onUpdate,
        palette: "success",
        disabled: e.hasUpdateDisabled
      }, {
        default: l(() => [
          g("Update ")
        ]),
        _: 1
      }, 8, ["onClick", "disabled"]), [
        [I, !e.loadingStatus.value && e.updateVisible]
      ])
    ]),
    "footer-button-delete": l(() => [
      B(y(a, {
        onClick: e.onDelete,
        palette: "danger",
        disabled: e.hasDeleteDisabled
      }, {
        default: l(() => [
          g("Delete ")
        ]),
        _: 1
      }, 8, ["onClick", "disabled"]), [
        [I, !e.loadingStatus.value && e.deleteVisible]
      ])
    ]),
    _: 2
  }, [
    e.hasCustomTabs ? void 0 : {
      name: "tab-crud-read",
      fn: l(() => [
        p(e.$slots, "read", { data: e.data })
      ]),
      key: "0"
    },
    C(e.customTabs, (h, c) => ({
      name: e.getTabSlotKey(c),
      fn: l(() => [
        !e.loadingStatus.value && e.hasData ? p(e.$slots, e.getTabSlotKey(c), {
          key: 0,
          data: e.data
        }) : f("", !0)
      ])
    }))
  ]), 1032, ["title", "pre-title", "palette", "size", "loading", "windowIndex", "tabs"]);
}
const Ue = /* @__PURE__ */ k(Ve, [["render", Le]]), He = {
  install: (e, t) => {
    e.component("lkt-window-canvas", oe).component("lkt-crud-window", Ue).component("lkt-alert-window", Be).component("lkt-confirm-window", je).component("lkt-choice-window", ze).component("lkt-window", z);
  },
  setCanvas: (e) => {
    $.canvas = e;
  }
};
export {
  Ee as addWindow,
  O as closeWindow,
  He as default,
  Ne as openWindow
};
