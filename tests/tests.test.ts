// test('register window', () => {
//   const expected = {
//     test_2: {
//       component: {},
//       alias: 'test',
//       index: "test_2",
//       key: 2,
//       props: {id: 2, name: 'testis', windowIndex: {name: 'test', key: 2}}
//     },
//     test_4: {
//       component: {},
//       alias: 'test',
//       index: "test_4",
//       key: 4,
//       props: {windowIndex: {name: 'test', key: 4}}
//     },
//     test2_1: {
//       component: {},
//       alias: 'test2',
//       index: "test2_1",
//       key: 1,
//       props: {windowIndex: {name: 'test2', key: 1}}
//     },
//   };
//
//   Controller.value.register('test', LktWindow);
//   Controller.value.register('test2', LktWindow);
//   Controller.value.open('test', 2, {id: 2, name: 'testis'});
//   Controller.value.open('test', 4);
//   Controller.value.open('test2', 1);
//
//
//   expect(JSON.stringify(Controller.value.components)).toEqual(JSON.stringify(expected));
// });
//
//
// test('register window2', () => {
//
//   Controller.value.clear();
//
//
//   const expected = {
//     test_2: {
//       component: {},
//       alias: 'test',
//       index: "test_2",
//       key: 2,
//       props: {id: 2, name: 'testis', windowIndex: {name: 'test', key: 2}}
//     },
//     test_4: {
//       component: {},
//       alias: 'test',
//       index: "test_4",
//       key: 4,
//       props: {windowIndex: {name: 'test', key: 4}}
//     },
//   };
//
//   Controller.value.register('test', LktWindow);
//   Controller.value.open('test', 2, {id: 2, name: 'testis'});
//   Controller.value.open('test', 4);
//   Controller.value.open('test2', 1);
//
//
//   expect(JSON.stringify(Controller.value.components)).toEqual(JSON.stringify(expected));
// });